import alerts from '../styles/alert.module.css'
import cn from 'classnames'

const Alert = ({ children, type }) => {
  return (
    <div
      className={cn({
        [alerts.success]: type === 'success',
        [alerts.error]: type === 'error'
      })}
    >
      {children}
    </div>
  )
}

export default Alert