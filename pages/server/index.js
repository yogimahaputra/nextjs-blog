export default function index({thisData}) {
    console.log(thisData)

    return (
        <div>
            this is serverside
            {thisData.map((e,index) => {
                return <p key={index}>{e.name}</p>
            })}
        </div>
    )
}

export async function getServerSideProps(){
    
    let res = await fetch(`http://backendexample.sanbercloud.com/api/student-scores`)
    let thisData = await res.json()

    return{
        props: { thisData }
    }
}