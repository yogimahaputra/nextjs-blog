export default function index({thisData}) {
    console.log(thisData)
    return (
        <div>
            this is static 
            {thisData.map((e,index) => {
                return <p key={index}>{e.name}</p>
            })}
        </div>
    )
}

export async function getStaticProps(){
    
    let res = await fetch(`http://backendexample.sanbercloud.com/api/student-scores`)
    let thisData = await res.json()

    return{
        props: { thisData },
        revalidate: 10,
    }
}