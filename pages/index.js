import React from 'react'
import Layout from '../components/layout'
import homeStyles from "../styles/Home.module.css"
import Link from "next/link"

export default function Home({ thisData }) {
  console.log(thisData)
  return (
    <Layout>
      <div className={homeStyles.left}>
        <Link href="/static"><a>Klik Me for static Page</a></Link>
      </div>
      <div className={homeStyles.right}>
        <Link href="/server"><a>Klik Me for serverside Page</a></Link>
      </div>
    </Layout>
  )
}

export async function getStaticProps(){
    
  let res = await fetch(`http://backendexample.sanbercloud.com/api/student-scores`)
  let thisData = await res.json()

  return{
      props: { thisData }
  }
}
